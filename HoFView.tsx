import HallOfFame, {HOFEntry} from './HallOfFame'
import React from 'react'
import { connect } from 'react-redux'
import {Text, View} from "react-native";

function HoFView({hallOfFame}: {hallOfFame: Array<HOFEntry>}) {
  return (<View>
    <Text>Hall Of Fame</Text>
    <HallOfFame entries={hallOfFame}/>
  </View>);
}

const mapStateToProps = (state: any) => {
  return {
    hallOfFame: state.hof.hallOfFame
  }
}

export default connect(mapStateToProps)(HoFView)