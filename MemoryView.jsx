import React, {Dispatch, useEffect} from 'react';
import GuessCount from './GuessCount';
import Card from './Card';
import HallOfFame from './HallOfFame';
import { connect } from 'react-redux';
import HighScoreInput from './HighScoreInput'
import { View, StyleSheet } from 'react-native';
import {fetchCards} from "./actions/cards";

class MemoryView extends React.Component {

  componentDidMount() {
      this.props.dispatch(fetchCards());
  }

  getFeedbackForCard(index) {
    const { currentPair, matchedCardIndices } = this.props;
    const indexMatched = matchedCardIndices.includes(index)

    if (currentPair.length < 2) {
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
    }

    if (currentPair.includes(index)) {
      return indexMatched ? 'justMatched' : 'justMismatched'
    }

    return indexMatched ? 'visible' : 'hidden'
  }

  render() {
    const { guesses, cards, hallOfFame, matchedCardIndices, scoreStored } = this.props;
    const won = matchedCardIndices.length > 0
      && matchedCardIndices.length === cards.length;
    return (<View style={styles.memory}>
      <GuessCount guesses={guesses}/>
      <View style={styles.cards}>
        {cards.map((card, index) => (
          <Card
            card={card}
            feedback={this.getFeedbackForCard(index)}
            index={index}
            key={index}
          />
        ))}
      </View>
      {won &&
      (scoreStored ? (
        <HallOfFame entries={hallOfFame}/>
      ) : (
        <HighScoreInput
          guesses={guesses}
        />
      ))}
    </View>);
  }
}

const styles = StyleSheet.create({
  memory: {
    margin: 40,
    flex: 1,
    backgroundColor: '#fff',
    alignContent: "center"
  },
  cards: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
  }
});

const mapStateToProps = (state) => {
  return {
    cards: state.cards.cards,
    currentPair: state.cards.currentPair,
    matchedCardIndices: state.cards.matchedCardIndices,
    guesses: state.cards.guesses,
    hallOfFame: state.hof.hallOfFame,
    scoreStored: state.hof.scoreStored
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemoryView)
