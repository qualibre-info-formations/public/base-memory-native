import React, { Dispatch } from 'react'
import { connect } from 'react-redux'

import { handleCardClick } from './actions/cards'
import {StyleSheet, TouchableOpacity, Text, View} from "react-native";

const HIDDEN_SYMBOL = '❓'

interface CardProps {
  card: string,
  feedback: string,
  index: number,
  dispatch: Function
}

const Card = ({ card, feedback, index, dispatch }: CardProps) => (
// @ts-ignore
  <TouchableOpacity style={[styles.card, styles[feedback]]} onPress={() => dispatch(handleCardClick(index))}>
    <View style={{flex: 1, justifyContent: "center"}}>
      <Text style={styles.symbol}>
          {feedback === 'hidden' ? HIDDEN_SYMBOL : card}</Text>
    </View>
  </TouchableOpacity>
)

const mapDispatchToProps = (dispatch: Dispatch<any>) => {
  return {
    dispatch: (action: Dispatch<any>) => { dispatch(action) }
  }
}

const styles = StyleSheet.create({
    card: {
        margin: 10,
        width: 50,
        height: 70,
    },
    hidden: {
        backgroundColor: 'silver'
    },
    justMatched: {
        borderColor: 'green',
        borderWidth: 3
    },
    justMismatched:  {
        borderColor: 'red',
        borderWidth: 3
    },
    symbol: {
        textAlign: 'center',
    }
})

export default connect(null, mapDispatchToProps)(Card)
