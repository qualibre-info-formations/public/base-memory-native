import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MemoryView from '../MemoryView';
import HoFView from '../HoFView'

export default function Routes() {
  return (
    <Switch>
      <Route path="/halloffame" component={HoFView} />
      <Route>
        <MemoryView />
      </Route>
    </Switch>
  );
}