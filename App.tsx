import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {StyleSheet, Text, View, Button, TextInput} from 'react-native';
import MemoryView from "./MemoryView";
import { Provider } from 'react-redux';
import Store from './store/configureStore';

export default function App() {
  return (
      <Provider store={Store}>
        <View style={styles.container}>
          <MemoryView />
          <StatusBar style="auto" />
        </View>
      </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
