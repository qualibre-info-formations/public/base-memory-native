import { getHOFEntries } from '../HallOfFame'

export const fetchHoF = () => {
  return (dispatch) => {
    dispatch({type: "STORE_HOF", hallOfFame: getHOFEntries()})
  }
}
