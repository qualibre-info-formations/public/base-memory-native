import shuffle from 'lodash.shuffle'

const SIDE = 4
export const API_KEY = '5d796281b5eedc635380d9b4'
const VISUAL_PAUSE_MSECS = 750

export const fetchCards = () => {
  return (dispatch) => {
    fetch('https://memoryhalloffame-9cee.restdb.io/rest/cards', {
      headers: {
        'cache-control': 'no-cache',
        'x-apikey': API_KEY,
      },
    })
      .then((result) => {
        if (result.ok) {
          return result.json()
        }
      })
      .then((data) => {
        let symbols = data.map((el) => el.symbol)
        symbols = shuffle(symbols)
        symbols = symbols.slice(0, (SIDE * SIDE) / 2)
        let newCards = symbols.concat(symbols)
        newCards = shuffle(newCards)
        dispatch(storeCards(newCards))
      })
      .catch((err) => {
        console.error(err)
      })
  }
}

export const handleCardClick = (index) => {
  return (dispatch, getState) => {
    const { currentPair, matchedCardIndices, cards } = getState().cards

    if (currentPair.length === 2 || currentPair.includes(index) || matchedCardIndices.includes(index)) {
      return
    }

    dispatch(storeInCurrentPair(index))
    if (currentPair.length === 0) {
      return
    }

    const newPair = [currentPair[0], index]
    const matched = cards[newPair[0]] === cards[newPair[1]]
    dispatch(increaseGuesses())
    if (matched) {
      dispatch(storeInMatchedCardIndices(newPair))
    }
    setTimeout(() => dispatch(clearCurrentPair()), VISUAL_PAUSE_MSECS)
  }
}

export const storeCards = (cards) => {
  return { type: "STORE_CARDS", cards }
}

export const increaseGuesses = () => {
  return { type: "INCREASE_GUESSES" }
}

export const storeInMatchedCardIndices = (cards) => {
  return { type: "STORE_MATCHED", cards }
}

export const storeInCurrentPair = (index) => {
  return { type: "STORE_IN_CURRENT_PAIR", index }
}

export const clearCurrentPair = () => {
  return { type: "CLEAR_CURRENT_PAIR" }
}
