import React, { useState, useEffect, FunctionComponent } from 'react'
import Geocode from "react-geocode"
import {Table, Row, Rows} from 'react-native-table-component'
import {StyleSheet} from "react-native";
import {API_KEY} from "./actions/cards";

export interface HOFEntry {
  date: string,
  guesses: number,
  id: number,
  player: string,
  coords?: string
}

interface HallOfFameProps {
  entries: Array<HOFEntry>
}

const HallOfFame:FunctionComponent<HallOfFameProps> = ({ entries }) => {
  // This geolocation state will store geocode for all entries
  const initialGeolocation: Array<any> = [];
  const [geolocation, setGeolocation] = useState(initialGeolocation);
  const tableHead = ['Date', 'Score', 'Player', 'Country'];
  // Precise coordinates are stored in the localStorage, we want to only show
  // country in the hall of fame
  useEffect(() => {
    async function fetchLoc() {
      Geocode.setApiKey('AIzaSyAFl272ksaGhUJImdAjEbEDcH6rmwPla44');
      const newGeoLoc = [];
      for (let i = 0; i < entries.length; i++) {
        const { coords } = entries[i];
        newGeoLoc[i] = undefined;
        if (coords) {
          const jsonCoords = JSON.parse(coords);
          newGeoLoc[i] = await Geocode.fromLatLng(jsonCoords.latitude, jsonCoords.longitude);
        }
      }
      return newGeoLoc;
    }
    fetchLoc().then((r: any[]) => setGeolocation(r));
  }, [entries]);
  const tableData = entries.map((el, index) => [ el.date, el.guesses, el.player, geolocation[index] &&
        `${geolocation[index].results[0].address_components
            .find((el: any) => el.types.includes('country')).long_name}`
  ]);
  return (
      <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
        <Row data={tableHead} style={styles.head} textStyle={styles.text}/>
        <Rows data={tableData} textStyle={styles.text}/>
      </Table>
  );
}

export default HallOfFame

const styles = StyleSheet.create({
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 6 }
});

// == Internal helpers ==============================================

const HOF_KEY = '::Memory::HallofFame'

export function getHOFEntries() {
  return JSON.parse(localStorage.getItem(HOF_KEY) || '[]');
}

export function saveHOFEntry(entry: HOFEntry, onStored: Function) {
  entry.date = new Date().toLocaleDateString()
  entry.id = Date.now()

  fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
    method: "POST",
    body: JSON.stringify(entry),
    headers: {
      'Content-Type': 'application/json',
      'x-apikey': API_KEY,
    },
  })
      .then((result) => {
        if (result.ok) {
          return result.json()
        }
      })
      .then(() => {
        fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
          headers: {
            'cache-control': 'no-cache',
            'x-apikey': API_KEY,
          },
        })
            .then((result) => {
              if (result.ok) {
                return result.json()
              }
            })
            .then((data) => {
              onStored(data)
            })
            .catch((err) => {
              console.error(err)
            })
      })
      .catch((err) => {
        console.error(err)
      })
}
