import React, {Dispatch, FunctionComponent, useEffect, useState} from 'react'
import {Button, View, Text, TextInput, ActivityIndicator} from 'react-native'
import {connect, MapDispatchToPropsFunction} from 'react-redux'
import * as Location from 'expo-location';

import {HOFEntry, saveHOFEntry} from './HallOfFame'
import {LocationObject} from "expo-location";

const HighScoreInput:FunctionComponent<{dispatch: Dispatch<any>, guesses: number}> = ({dispatch, guesses}) => {
  const [winner, setWinner] = useState('')
  const [location, setLocation] = useState<LocationObject>();
  const [errorMsg, setErrorMsg] = useState('');
  const [storingInProgress, setStoringInProgress] = useState(false);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  // Arrow fx for binding
  const handleWinnerUpdate = (text: string) => {
    setWinner(text.toUpperCase())
  }

  // Arrow fx for binding
  const persistWinner = () => {
    setStoringInProgress(true);
    const newEntry: HOFEntry = { date: '', id: 0, guesses, player: winner };
    if (location && location.coords) {
      newEntry.coords = JSON.stringify({
        latitude: location.coords.latitude,
        longitude: location.coords.longitude
      })
    }
    saveHOFEntry(newEntry, (entries: Array<HOFEntry>) => {
      setStoringInProgress(false);
      dispatch({type: 'STORE_HOF', hallOfFame: entries})
      dispatch({type: 'SET_SCORE_STORED', scoreStored: true})
    })
  }
  return (
    <View>
      <View>
          <Text>Bravo! Entre ton prénom :</Text>
            <TextInput
              onChangeText={handleWinnerUpdate}
              value={winner}
            />
        {storingInProgress ? <ActivityIndicator size="large" /> : <Button onPress={persistWinner} title="J'ai gagné !" />}
      </View>
      <View><Text>Localisation : {location && `${location.coords.latitude}, ${location.coords.longitude}`}</Text></View>
      <View><Text>{errorMsg && `Location error : ${errorMsg}`}</Text></View>
    </View>
  )
}

const mapDispatchToProps:MapDispatchToPropsFunction<any, any> = (dispatch: any) => {
  return {
    dispatch: (action: any) => dispatch(action)
  }
}

export default connect(null, mapDispatchToProps)(HighScoreInput)
