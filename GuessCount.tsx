import React, {FunctionComponent} from 'react';
import {Text, View} from "react-native";

const GuessCount: FunctionComponent<{guesses: number}> = ({ guesses }) => <View style={{height: 60}}><Text>{guesses}</Text></View>

export default GuessCount
