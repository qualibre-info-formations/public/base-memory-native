import { createStore, applyMiddleware, combineReducers } from 'redux'
import storeCards from './reducers/cardsReducer';
import storeHallOfFame from './reducers/hofReducer'
import thunk from "redux-thunk";

export default createStore(combineReducers({cards: storeCards, hof: storeHallOfFame}), applyMiddleware(thunk))
