const initialState = { cards: [], matchedCardIndices: [], currentPair: [], guesses: 0 };

export default function storeCards(state = initialState, action) {
  switch (action.type) {
    case 'STORE_CARDS':
      return {...state, cards: action.cards};
    case 'CLEAR_CURRENT_PAIR':
      return {...state, currentPair: []};
    case 'INCREASE_GUESSES':
      return {...state, guesses: state.guesses + 1};
    case 'STORE_MATCHED':
      return {...state, matchedCardIndices: [...state.matchedCardIndices, ...action.cards]};
    case 'STORE_IN_CURRENT_PAIR':
      return {...state, currentPair: [...state.currentPair, action.index]};
    default:
      return state
  }
}
