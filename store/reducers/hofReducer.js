const initialState = { hallOfFame: [], scoreStored: false };

export default function storeHallOfFame(state = initialState, action) {
  switch (action.type) {
    case 'STORE_HOF':
      return {...state, hallOfFame: action.hallOfFame};
    case 'SET_SCORE_STORED':
      return {...state, scoreStored: action.scoreStored};
    default:
      return state
  }
}
